package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/spf13/viper"
	"github.com/vartanbeno/go-reddit/v2/reddit"
)

var ctx = context.Background()
var allPosts []*reddit.Post

type Item struct {
	URL       string
	Permalink string
}

func appendDataToFile(line *reddit.Post, outputFile string) {
	f, err := os.OpenFile(outputFile,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	if _, err := f.WriteString(line.Permalink + "\n"); err != nil {
		log.Println(err)
	}
	if _, err := f.WriteString(line.URL + "\n"); err != nil {
		log.Println(err)
	}
	fmt.Printf("Saved - %s -- to -- %s \n", line.Title, outputFile)
}

// We get all of the saved posts in an array that can be processed in the CLI
func getSavedPosts(client *reddit.Client, next string) (posts []*reddit.Post, after string) {
	data, _, nextPage, _ := client.User.Saved(ctx, &reddit.ListUserOverviewOptions{
		ListOptions: reddit.ListOptions{
			Limit: *reddit.Int(2),
			After: next,
		},
	})
	allPosts = append(allPosts, data...)
	// This returns the next page for processing
	after = nextPage.After
	return data, after
}

// Gets a post ID and unsaves it
func unsavePost(client *reddit.Client, post *reddit.Post) (string, error) {
	_, err := client.Post.Unsave(ctx, post.FullID)
	if err != nil {
		fmt.Println("Failed to unsave post - ", err)
	}

	return post.FullID, err
}

func saveSavedPostsToFile(client *reddit.Client, next string) {

	// We get the saved posts and the next page(after)
	data, after := getSavedPosts(client, next)

	for _, savedPost := range data {
		appendDataToFile(savedPost, "exported.txt")
	}

	// We return if there are no other pages
	if next == "" {
		return
	}
	// We recursively call the function until there are no other pages(after)
	saveSavedPostsToFile(client, after)
}

func main() {

	viper.SetConfigName("config")
	viper.SetConfigType("toml")

	viper.AddConfigPath(".") // optionally look for config in the working directory

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("Fatal error config file: %w \n", err))
	}

	credentials := reddit.Credentials{
		ID:       viper.GetString("config.APP_ID"),
		Secret:   viper.GetString("config.APP_SECRET"),
		Username: viper.GetString("config.username"),
		Password: viper.GetString("config.password"),
	}
	client, err := reddit.NewClient(credentials)

	if err != nil {
		panic(err)
	}

	// Save the posts to file
	saveSavedPostsToFile(client, "nil")

	// Unsave all of the posts

	// Uncomment in case you want to unsave the posts
	// for _, post := range allPosts {
	// 	_, err := unsavePost(client, post)

	// 	if err != nil {
	// 		fmt.Println(err)
	// 	} else {
	// 		fmt.Println("Unsaved - ", post.FullID)

	// 	}
	// }
}
