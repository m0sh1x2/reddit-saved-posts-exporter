# Reddit Save to file

This is a very simple script written in Go that exports all reddit saves to a file.

It can also unsave everything in case you want to clear your account.

The functionality can be used to clear any comments upvotes, downvotes or whatever you want.

# Setup

- Install the Go modules `go mod tidy`
- Set up a Script Reddit App and get the ID and Secret from here - https://ssl.reddit.com/prefs/apps/
- Set up the credentials in the `config.toml` file
- Run `go run main.go`

Disclaimer: This is just a test script and I do not provide any warranty of it's usage(In case you delete all of your saved date without understanding on how to use it)